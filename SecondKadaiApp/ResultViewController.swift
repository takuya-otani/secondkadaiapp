//
//  ResultViewController.swift
//  SecondKadaiApp
//
//  Created by Takuya Otani on 1/02/17.
//  Copyright © 2017 takuya.otani. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

  @IBOutlet weak var label: UILabel!

  var name: String = ""

  override func viewDidLoad() {
    super.viewDidLoad()

    if (!self.name.isEmpty) {
      label.text = "こんにちわ、\(self.name)さん"
    }
    else {
      label.text = "こんにちわ"
    }
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

}

//
//  ViewController.swift
//  SecondKadaiApp
//
//  Created by Takuya Otani on 26/01/17.
//  Copyright © 2017 takuya.otani. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var input: UITextField!

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let resultViewController:ResultViewController = segue.destination as! ResultViewController
    resultViewController.name = input.text!
  }

  @IBAction func unwind(segue : UIStoryboardSegue) {
  }
  
}

